const express = require('express')
const cors = require('cors');
const config = require('./src/config')
const admin = require('firebase-admin')

const app = express()
app.use(cors());
app.use(express.json())

//Acces to firebase
const  serviceAccount = require("./test-fer-3526b-firebase-adminsdk-8dko5-008714fd51.json");

//settings
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: 'https://test-fer-3526b.firebaseio.com/'
})

//routers
app.use('/mutation', require('./src/routes/mutation'))
app.use('/sendEmail', require('./src/routes/sendEmail'))
app.use('/stats', require('./src/routes/stats'))

app.listen(config.PORT, config.HOST)



