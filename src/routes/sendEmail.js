const express = require('express')
const router = express.Router()
const fs = require('fs');
const nodeMailer = require('nodemailer');

router.post('/', (req, res, next) => {
    // Get html to send in the mail body
    const html = fs.readFileSync('./assets/3-validacion-cuenta.html', 'utf8');

    //get email to send
    const emailToSend = req.body.email
    
    //Protocol and authentication data
    var transporter = nodeMailer.createTransport({
        service: 'gmail',
        host: 'smtp.gmail.com ',
        port: 465,
        secure: false,
        requireTLS: true,
        auth: {
            user: 'ferbiagettitest@gmail.com',
            pass: 'Fernando2009',
        },
    });

    // Mail structure data
    const mailOptions = {
        from: 'pruebaCandidato@gmail.com', // sender address
        to: [emailToSend], // list of receivers
        subject: 'Test Project', // Subject line
        html: html //Html structure
    };

    //Send Email
    transporter.sendMail(mailOptions, function (err, info) {
        if(err)
            res.json({status: err.responseCode, message: err.message})
        else
            res.json({status: 200, message: 'email enviado correctamente'})
    });
});


module.exports = router