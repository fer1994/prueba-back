const express = require('express')
const router = express.Router()
const admin = require('firebase-admin')

const db = admin.database();

router.get('/', async (req, res, next) => {
    
    //Search the length of ADN MUTANT
    const mutant = (await db.ref('count_mutations').once('value')).exportVal();
    const lengthMutant = Object.keys(mutant).length;

    //Search the length of ADN NO MUTANT
    const noMutant = (await db.ref('count_no_mutation').once('value')).exportVal();
    const lengthNoMutant = Object.keys(noMutant).length;

    res.json({ status: 200, 
        data: {'count_mutations': lengthMutant, 'count_no_mutation': lengthNoMutant, 'ratio': lengthMutant/lengthNoMutant}  })

});

module.exports = router