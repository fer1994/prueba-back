const express = require('express')
const router = express.Router()
const admin = require('firebase-admin')

const db = admin.database();

/**Check if matrix have mutations */
router.post('/', (req, res, next) => {
    //obtaing request body
    const dnaList = req.body.dna;

    //Parse array to matrix
    const matrizDna = parseToMatrix(dnaList);

    //Check only use A,T,C,G
    if(!matrizDna) {
        res.json({ status: 403, mutation: false, data: 'ADN solo pueden ser (A,T,C,G)' })
        res.end() 
        return;
    }

    //Check how many mutation have the horizontal matrix
    const horitonzalMut = hasMutationHorizontal(matrizDna);
    //Check how many mutation have the vertical matrix
    const verticalMut = hasMutationVertical(matrizDna)
    //Check how many mutation have the oblique matrix
    const obliqueMut = hasMutationOblique(matrizDna);
    //check if have a inverted oblique mutation
    const invertedObliqueMutation = hasMutationInverted(matrizDna);

    //Count Total Mutations;
    const totalMutation = horitonzalMut + verticalMut + obliqueMut + invertedObliqueMutation;

    if(totalMutation > 1 ){
        db.ref('count_mutations').push(1)
        res.json({ status: 200, mutation: true })
        res.end()
        return;
    }

    db.ref('count_no_mutation').push(1)
    res.json({ status: 403, mutation: false })
    res.end()
})

const parseToMatrix = ( dnaList => {
    const matrizMutation = [];
    for (var i = 0; i < dnaList.length; i++) {
        let stringList = Array.from(dnaList[i]);
        if( stringList.includes('B') ||
            stringList.includes('D') ||
            stringList.includes('E') ||
            stringList.includes('F') ||
            stringList.includes('H') ||
            stringList.includes('I') ||
            stringList.includes('J') ||
            stringList.includes('K') ||
            stringList.includes('L') ||
            stringList.includes('M') ||
            stringList.includes('N') ||
            stringList.includes('Ñ') ||
            stringList.includes('O') ||
            stringList.includes('P') ||
            stringList.includes('Q') ||
            stringList.includes('R') ||
            stringList.includes('S') ||
            stringList.includes('U') || 
            stringList.includes('V') ||
            stringList.includes('W') ||
            stringList.includes('X') ||
            stringList.includes('Y') ||
            stringList.includes('Z') ) {
                return false;
            }

        matrizMutation.push(stringList);
    };
    return matrizMutation;
});


/**Obtaing inverted oblique mutation */
const hasMutationInverted = (dnaMatrix => {

    //inverted the matrix
    dnaMatrix.forEach(matrix => {
        return matrix.reverse()
    });

    //use the same function to calculcate inverted matrix
    return hasMutationOblique(dnaMatrix);
})


/* Obtaing the oblique mutation */
const hasMutationOblique = ( dnaMatrix => {
    let mutationCounter = 0;
    diagIndex = dnaMatrix.length - 4;
    //Search dilog index
    for(let x=0-diagIndex;x<=diagIndex;x++){
        let chain=0
        let previousChar=null

        //Horizontal Column
        for(let i=0;i<dnaMatrix.length;i++){

            //Vertical Column
            for(let j=0;j<dnaMatrix[i].length;j++){     
                
                //Check if in the diagonal
                if(i==j-x){
                    if ( (!previousChar || previousChar === dnaMatrix[i][j])){
                        chain++
                        if (chain === 4){
                            mutationCounter++;
                        }
                    } else {
                        chain = 1;
                    }
                    previousChar = dnaMatrix[i][j]
                }
            }
        }
    }
    return mutationCounter;
});

/** Check mutation in vertical matrix */
const hasMutationVertical = ( dnaMatrix => {
    let mutationCounter = 0;
    for (var colIndex = dnaMatrix[0].length - 1; colIndex >= 0; colIndex--) {
        let previousChar = null
        let chain = 0
        for (var rowIndex = dnaMatrix.length - 1; rowIndex >= 0; rowIndex--) {
            if (!previousChar || previousChar === dnaMatrix[rowIndex][colIndex]){
                chain++;
                if (chain === 4){
                    mutationCounter++;
                }
            } else {
                chain = 1;
            }
            previousChar = dnaMatrix[rowIndex][colIndex]
        }
    }
    return mutationCounter;
})

/** Check mutation horizontal matrix */
const hasMutationHorizontal = (dnaMatrix => {
    let mutationCounter = 0;
    let previousChar = null
    let chain = 0
    for (var rowIndex = dnaMatrix.length - 1; rowIndex >= 0; rowIndex--) {
        for (var colIndex = dnaMatrix[0].length - 1; colIndex >= 0; colIndex--) {
        if (!previousChar || previousChar === dnaMatrix[rowIndex][colIndex]){
            chain++;
            if (chain === 4){
                mutationCounter++;
            }
        } else {
            chain = 1;
        }
        previousChar = dnaMatrix[rowIndex][colIndex]
        }
    }
    return mutationCounter;
})

module.exports = router